package neversiteupDao

import (
	"fmt"
	"neversiteup/database"
	authModels "neversiteup/modules/models"
)

func LoginDao(req authModels.LoginRequest) (resp []authModels.LoginResponse, errRep error) {
	pgClientPool := database.GetPostgres()
	statment := `
	SELECT * FROM "public".users WHERE username = $1 and password= $2
	`
	res, err := pgClientPool.Query(statment, req.Username, req.Password)

	if err != nil {
		errRep = err
		return
	}
	defer res.Close()
	for res.Next() {
		var response authModels.LoginResponse
		errNext := res.Scan(
			&response.Id,
			&response.Username,
			&response.FirstName,
			&response.LastName,
			&response.Phone,
			&response.Address,
			&response.Email,
			&response.Token,
			&response.Password,
		)
		if errNext != nil {
			errRep = errNext
			res.Close()
			return
		}
		resp = append(resp, response)
	}
	return
}

func RegisterDao(req authModels.RegisterRequest) (errRep error) {
	pgClientPool := database.GetPostgres()
	sqlStatement := `
    	INSERT INTO "public".users (
		firstname, lastname, username, password, email, phone, token, address)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
   		RETURNING id
	`
	id := 0
	err := pgClientPool.QueryRow(sqlStatement, req.FirstName, req.LastName, req.Username, req.Password, req.Email, req.Phone, "", req.Address).Scan(&id)
	if err != nil {
		return err
	}
	fmt.Println("New record ID is:", id)
	return nil

}
