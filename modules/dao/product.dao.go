package neversiteupDao

import (
	"context"
	"log"
	"neversiteup/database"
	productModels "neversiteup/modules/models"
	"neversiteup/modules/utils"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func GetProductDao() ([]productModels.ProductResponse, error) {
	var dataproduct []productModels.ProductResponse
	mongodb := database.GetMongoDbPool()
	cursor, err := mongodb.Collection("product").Find(context.TODO(), bson.M{})
	if err != nil {
		logError := utils.InitErrorMsgWithCampaignCode("GET PRODUCTS FAILED", "", err.Error())
		log.Println(logError)
		return nil, err

	}
	err = cursor.All(context.TODO(), &dataproduct)

	if err != nil {
		// fmt.Println(err)
		if err == mongo.ErrNoDocuments {
			logError := utils.InitErrorMsgWithCampaignCode("CONVERT DATA FAILED", "", err.Error())
			log.Println(logError)
			return nil, err

		} else {
			logError := utils.InitErrorMsgWithCampaignCode("MIGRATE_CAMPAIGN|GetCampaignShop|ERROR|", "", err.Error())
			log.Println(logError)
			return nil, err
		}
	}
	return dataproduct, nil

}

func GetProductOneDao(reqMongo productModels.ProductRequest) ([]productModels.ProductResponse, error) {
	var dataproduct []productModels.ProductResponse
	mongodb := database.GetMongoDbPool()
	filter := reqMongo
	cursor, err := mongodb.Collection("product").Find(context.TODO(), filter)
	if err != nil {
		logError := utils.InitErrorMsgWithCampaignCode("GET PRODUCTS FAILED", "", err.Error())
		log.Println(logError)
		return nil, err
	}
	err = cursor.All(context.TODO(), &dataproduct)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			logError := utils.InitErrorMsgWithCampaignCode("CONVERT DATA FAILED NO DOCUMENT", "", err.Error())
			log.Println(logError)
			return nil, err

		} else {
			logError := utils.InitErrorMsgWithCampaignCode("CONVERT DATA FAILED", "", err.Error())
			log.Println(logError)
			return nil, err
		}
	}
	return dataproduct, nil

}

func CreateProductOneDao(reqMongo productModels.ProductRequest) ([]productModels.ProductResponse, error) {
	var dataproduct []productModels.ProductResponse
	mongodb := database.GetMongoDbPool()
	filter := reqMongo
	_, err := mongodb.Collection("product").InsertOne(context.TODO(), filter)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			logError := utils.InitErrorMsgWithCampaignCode("CONVERT DATA FAILED NO DOCUMENT", "", err.Error())
			log.Println(logError)
			return nil, err

		} else {
			logError := utils.InitErrorMsgWithCampaignCode("CONVERT DATA FAILED", "", err.Error())
			log.Println(logError)
			return nil, err
		}
	}
	return dataproduct, nil

}
