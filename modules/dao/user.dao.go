package neversiteupDao

import (
	"context"
	"log"
	"neversiteup/database"
	userModel "neversiteup/modules/models"
	userModels "neversiteup/modules/models"
	"neversiteup/modules/utils"

	"go.mongodb.org/mongo-driver/mongo"
)

func GetUserDao() (resp []userModels.UsersResponse, errRep error) {
	pgClientPool := database.GetPostgres()
	statment := `
	SELECT id , username, firstname, lastname, phone, address, email FROM "public".users
	`
	res, err := pgClientPool.Query(statment)

	if err != nil {
		errRep = err
		return
	}
	defer res.Close()
	for res.Next() {
		var response userModel.UsersResponse
		errNext := res.Scan(
			&response.Id,
			&response.Username,
			&response.FirstName,
			&response.LastName,
			&response.Phone,
			&response.Address,
			&response.Email,
		)
		if errNext != nil {
			errRep = errNext
			res.Close()
			return
		}
		resp = append(resp, response)
	}
	return

}

func GetUserOneDao(reqMongo userModels.UserRequest) ([]userModels.UsersResponse, error) {
	var dataUser []userModels.UsersResponse
	mongodb := database.GetMongoDbPool()
	filter := reqMongo
	cursor, err := mongodb.Collection("users").Find(context.TODO(), filter)
	if err != nil {
		logError := utils.InitErrorMsgWithCampaignCode("GET USERS FAILED", "", err.Error())
		log.Println(logError)
		return nil, err
	}
	err = cursor.All(context.TODO(), &dataUser)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			logError := utils.InitErrorMsgWithCampaignCode("CONVERT DATA FAILED NO DOCUMENT", "", err.Error())
			log.Println(logError)
			return nil, err

		} else {
			logError := utils.InitErrorMsgWithCampaignCode("CONVERT DATA FAILED", "", err.Error())
			log.Println(logError)
			return nil, err
		}
	}
	return dataUser, nil

}
