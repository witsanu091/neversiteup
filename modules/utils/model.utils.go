package utils

type ResponseStandard struct {
	Code     string      `json:"code"`
	BizError string      `json:"bizError"`
	Message  string      `json:"message"`
	Data     interface{} `json:"data,omitempty"`
	Error    *[]Error    `json:"error,omitempty"`
}

type Error struct {
	Param   string `json:"param"`
	Message string `json:"message"`
}

type Result struct {
	Code        string
	Description string
}

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Token string `json:"token"`
}

type User struct {
	ID             int
	Email          string
	Password       string
	FavoritePhrase string
}
