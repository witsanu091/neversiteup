package utils

import (
	"errors"

	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
)

const (
	SUCCESS_CODE = "200"
	SUCCESS_DESC = "SUCCESS"
	FAIL_CODE    = "500"
	FAIL_DESC    = "FAILED"
	NULL_CODE    = "400"
	NULL_MESSEGE = "DATA NOT FOUND"
)

func InitErrorMsgWithCampaignCode(serviceName string, code string, errorMsg string) string {
	return serviceName + code + "|" + errorMsg
}

func ResponseSuccess(data interface{}) ResponseStandard {
	return ResponseStandard{
		Code:     SUCCESS_CODE,
		Message:  SUCCESS_DESC,
		BizError: SUCCESS_DESC,
		Data:     data,
	}
}

func ResponseFailed(errorMsg string) ResponseStandard {
	return ResponseStandard{
		Code:     FAIL_CODE,
		Message:  FAIL_DESC,
		BizError: errorMsg,
	}
}

func ResponseDataNull() ResponseStandard {
	return ResponseStandard{
		Code:     NULL_CODE,
		Message:  NULL_MESSEGE,
		BizError: FAIL_DESC,
	}
}

func NewAuthMiddleware(secret string) fiber.Handler {
	return jwtware.New(jwtware.Config{
		SigningKey: []byte(secret),
	})
}

// Simulate a database call
func FindByCredentials(email, password string) (*User, error) {
	// Here you would query your database for the user with the given email

	if email == "test@mail.com" && password == "test12345" {
		return &User{
			ID:             1,
			Email:          "test@mail.com",
			Password:       "test12345",
			FavoritePhrase: "Hello, World!",
		}, nil
	}
	return nil, errors.New("user not found")
}

// func ValidateToken(token string) (*jwt.Token, error) {
// 	return jwt.Parse(token, func(t_ *jwt.Token) (interface{}, error) {
// 		if _, ok := t_.Method.(*jwt.SigningMethodHMAC); !ok {
// 			return nil, fmt.Errorf("Unexpected signing method %v", t_.Header["alg"])
// 		}
// 		return []byte(config.Secret), nil
// 	})
// }
