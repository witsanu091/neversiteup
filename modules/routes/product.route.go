package neversiteupRoute

import (
	productController "neversiteup/modules/controller"

	"github.com/gofiber/fiber/v2"
)

func ProductHandlerRoute(r fiber.Router) {
	r.Get("/products", productController.GetProduct)    //hint : /api/v1/users
	r.Get("/product", productController.GetProductOne)  //hint : /api/v1/user/id
	r.Post("/product", productController.CreateProduct) //hint : /api/v1/user
}
