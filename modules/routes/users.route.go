package neversiteupRoute

import (
	userController "neversiteup/modules/controller"

	"github.com/gofiber/fiber/v2"
)

func UserHandlerRoute(r fiber.Router) {
	r.Get("/users", userController.GetUsers)  //hint : /api/v1/users
	r.Get("/user", userController.GetUserOne) //hint : /api/v1/user/id
}
