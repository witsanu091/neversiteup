package neversiteupRoute

import (
	authController "neversiteup/modules/controller"

	"github.com/gofiber/fiber/v2"
)

func AuthHandlerRoute(r fiber.Router) {
	r.Post("/register", authController.RegisterController) //hint : /api/v1/register
	r.Post("/login", authController.LoginController)       //hint : /api/v1/login
}
