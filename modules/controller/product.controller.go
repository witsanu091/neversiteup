package neversiteupController

import (
	neversiteupModels "neversiteup/modules/models"
	neversiteupServices "neversiteup/modules/services"
	"neversiteup/modules/utils"

	"github.com/gofiber/fiber/v2"
)

func GetProduct(c *fiber.Ctx) error {

	response := neversiteupServices.GetProductService()

	return c.Status(fiber.StatusOK).JSON(response)

}
func GetProductOne(c *fiber.Ctx) error {
	var response utils.ResponseStandard
	req := new(neversiteupModels.ProductRequest)

	if err := c.BodyParser(req); err != nil {
		return c.Status(fiber.StatusOK).JSON(utils.ResponseFailed(err.Error()))
	}

	response = neversiteupServices.GetProductOneService(*req)

	return c.Status(fiber.StatusOK).JSON(response)

}

func CreateProduct(c *fiber.Ctx) error {
	var response utils.ResponseStandard
	req := new(neversiteupModels.ProductRequest)

	if err := c.BodyParser(req); err != nil {
		return c.Status(fiber.StatusOK).JSON(utils.ResponseFailed(err.Error()))
	}

	response = neversiteupServices.CreateProductService(*req)

	return c.Status(fiber.StatusOK).JSON(response)
}
