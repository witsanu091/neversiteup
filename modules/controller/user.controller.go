package neversiteupController

import (
	neversiteupModels "neversiteup/modules/models"
	neversiteupServices "neversiteup/modules/services"
	"neversiteup/modules/utils"

	"github.com/gofiber/fiber/v2"
)

func GetUsers(c *fiber.Ctx) error {

	response := neversiteupServices.GetUserService()

	return c.Status(fiber.StatusOK).JSON(response)

}
func GetUserOne(c *fiber.Ctx) error {
	var response utils.ResponseStandard
	req := new(neversiteupModels.UserRequest)

	if err := c.BodyParser(req); err != nil {
		return c.Status(fiber.StatusOK).JSON(utils.ResponseFailed(err.Error()))
	}

	response = neversiteupServices.GetUserOneService(*req)

	return c.Status(fiber.StatusOK).JSON(response)

}

func GetHello(c *fiber.Ctx) error {
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"message": "Success",
	})
}
