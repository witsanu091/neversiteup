package neversiteupController

import (
	authModels "neversiteup/modules/models"
	authService "neversiteup/modules/services"

	"github.com/gofiber/fiber/v2"
)

func LoginController(c *fiber.Ctx) error {
	loginRequest := new(authModels.LoginRequest)
	if err := c.BodyParser(loginRequest); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": err.Error(),
		})
	}
	response, err := authService.LoginService(*loginRequest)
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error": err.Error(),
		})
	}

	return c.Status(fiber.StatusOK).JSON(response)

}

// Protected route
// func Protected(c *fiber.Ctx) error {
// 	user := c.Locals("user").(*jtoken.Token)
// 	claims := user.Claims.(jtoken.MapClaims)
// 	email := claims["email"].(string)
// 	favPhrase := claims["fav"].(string)
// 	return c.SendString("Welcome 👋" + email + " " + favPhrase)
// }

func RegisterController(c *fiber.Ctx) error {
	registerRequest := new(authModels.RegisterRequest)
	if err := c.BodyParser(registerRequest); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": err.Error(),
		})
	}
	response, err := authService.RegisterService(*registerRequest)
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error ": err.Error(),
		})
	}

	return c.Status(fiber.StatusOK).JSON(response)

}
