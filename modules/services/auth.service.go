package neversiteupServices

import (
	authDao "neversiteup/modules/dao"
	authModels "neversiteup/modules/models"
	"time"

	config "neversiteup/configs"
	utils "neversiteup/modules/utils"

	jtoken "github.com/golang-jwt/jwt/v4"
)

func LoginService(req authModels.LoginRequest) (utils.ResponseStandard, error) {

	response := authModels.LoginResponse{}

	resp, err := authDao.LoginDao(req)
	if err != nil {
		return utils.ResponseFailed("error dao auth"), err
	}
	if len(resp) == 0 {
		return utils.ResponseFailed("user not found"), nil
	} else {
		dataUserResponse := &resp
		day := time.Hour * 24
		claims := jtoken.MapClaims{}

		for _, v := range *dataUserResponse {
			claims = jtoken.MapClaims{
				"Id":        v.Id,
				"email":     v.Email,
				"firstName": v.FirstName,
				"lastname":  v.LastName,
				"username":  v.Username,
				"address":   v.Address,
				"phone":     v.Phone,
				"exp":       time.Now().Add(day * 1).Unix(),
			}
		}

		token := jtoken.NewWithClaims(jtoken.SigningMethodHS256, claims)
		t, err := token.SignedString([]byte(config.Secret))
		if err != nil {
			return utils.ResponseFailed("can't set up token"), err
		}

		for _, v := range *dataUserResponse {
			response = authModels.LoginResponse{
				Id:        v.Id,
				Email:     v.Email,
				FirstName: v.FirstName,
				LastName:  v.LastName,
				Username:  v.Username,
				Address:   v.Address,
				Phone:     v.Phone,
				Token:     t,
			}
		}
	}

	return utils.ResponseSuccess(response), nil

}

func RegisterService(req authModels.RegisterRequest) (utils.ResponseStandard, error) {

	err := authDao.RegisterDao(req)
	if err != nil {
		return utils.ResponseFailed("error can't register"), err
	}

	return utils.ResponseSuccess("Register Success"), nil
}
