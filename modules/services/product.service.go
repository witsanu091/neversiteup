package neversiteupServices

import (
	productDao "neversiteup/modules/dao"
	neversiteupModels "neversiteup/modules/models"
	productModels "neversiteup/modules/models"
	"neversiteup/modules/utils"
)

func GetProductService() utils.ResponseStandard {
	var responseProduct productModels.ProductResponse
	var respProduct []productModels.ProductResponse

	resp, err := productDao.GetProductDao()
	if err != nil {
		return utils.ResponseFailed(err.Error())
	}
	for _, v := range resp {
		responseProduct.ProductName = v.ProductName
		responseProduct.Price = v.Price
		responseProduct.Amount = v.Amount
		responseProduct.Detail = v.Detail
		responseProduct.Category = v.Category
		responseProduct.Color = v.Color
		responseProduct.Status = v.Status
		responseProduct.Id = v.Id

		respProduct = append(respProduct, responseProduct)
	}
	if len(respProduct) == 0 {
		return utils.ResponseDataNull()
	} else {
		dataProductResponse := &productModels.DataProductResponse{respProduct}
		return utils.ResponseSuccess(dataProductResponse)

	}

}

func GetProductOneService(req productModels.ProductRequest) utils.ResponseStandard {
	var responseProduct productModels.ProductResponse
	var respProduct []productModels.ProductResponse

	resp, err := productDao.GetProductOneDao(req)
	if err != nil {
		return utils.ResponseFailed(err.Error())
	}
	for _, v := range resp {
		responseProduct.ProductName = v.ProductName
		responseProduct.Price = v.Price
		responseProduct.Amount = v.Amount
		responseProduct.Detail = v.Detail
		responseProduct.Status = v.Status
		responseProduct.Id = v.Id

		respProduct = append(respProduct, responseProduct)
	}
	if len(respProduct) == 0 {
		return utils.ResponseDataNull()
	} else {
		dataUserResponse := &productModels.DataProductResponse{respProduct}
		return utils.ResponseSuccess(dataUserResponse)

	}

}

func CreateProductService(req neversiteupModels.ProductRequest) utils.ResponseStandard {

	resp, err := productDao.CreateProductOneDao(req)
	if err != nil {
		return utils.ResponseFailed(err.Error())
	}

	return utils.ResponseSuccess(resp)

}
