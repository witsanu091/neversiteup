package neversiteupServices

import (
	usersDao "neversiteup/modules/dao"
	usersModels "neversiteup/modules/models"
	"neversiteup/modules/utils"
)

func GetUserService() utils.ResponseStandard {
	var responseUser usersModels.UsersResponse
	var respUser []usersModels.UsersResponse

	resp, err := usersDao.GetUserDao()
	if err != nil {
		return utils.ResponseFailed(err.Error())
	}
	for _, v := range resp {
		responseUser.Username = v.Username
		responseUser.FirstName = v.FirstName
		responseUser.LastName = v.LastName
		responseUser.Phone = v.Phone
		responseUser.Email = v.Email
		responseUser.Address = v.Address

		respUser = append(respUser, responseUser)
	}
	if len(respUser) == 0 {
		return utils.ResponseDataNull()
	} else {
		dataUserResponse := &usersModels.DataUserResponse{respUser}
		return utils.ResponseSuccess(dataUserResponse)

	}

}

func GetUserOneService(req usersModels.UserRequest) utils.ResponseStandard {
	var responseUser usersModels.UsersResponse
	var respUser []usersModels.UsersResponse

	resp, err := usersDao.GetUserOneDao(req)
	if err != nil {
		return utils.ResponseFailed(err.Error())
	}
	for _, v := range resp {
		responseUser.Username = v.Username
		responseUser.FirstName = v.FirstName
		responseUser.LastName = v.LastName
		responseUser.Phone = v.Phone
		responseUser.Email = v.Email
		responseUser.Address = v.Address

		respUser = append(respUser, responseUser)
	}
	if len(respUser) == 0 {
		return utils.ResponseDataNull()
	} else {
		dataUserResponse := &usersModels.DataUserResponse{respUser}
		return utils.ResponseSuccess(dataUserResponse)

	}

}
