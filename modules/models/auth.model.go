package neversiteupModels

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Id        string `json:"id,omitempty"`
	Username  string `json:"username,omitempty"`
	FirstName string `json:"firstname,omitempty" `
	LastName  string `json:"lastname,omitempty" `
	Phone     string `json:"phone,omitempty"`
	Address   string `json:"address,omitempty" `
	Email     string `json:"email,omitempty"`
	Token     string `json:"token,omitempty"`
	Password  string `json:"password,omitempty"`
}

type UserLoginResponse struct {
	Id        string `json:"id"`
	Username  string `json:"username"`
	FirstName string `json:"firstName" `
	LastName  string `json:"lastName" `
	Phone     string `json:"phone"`
	Address   string `json:"address" `
	Email     string `json:"email"`
}

type RegisterRequest struct {
	Username  string `json:"username" validate:"required"`
	FirstName string `json:"firstName" validate:"required" `
	LastName  string `json:"lastName" validate:"required"`
	Phone     string `json:"phone" validate:"required"`
	Address   string `json:"address" validate:"required"`
	Email     string `json:"email" validate:"required"`
	Password  string `json:"password" validate:"required"`
}
