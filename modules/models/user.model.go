package neversiteupModels

type UsersResponse struct {
	Id        int    `json:"id"`
	Username  string `json:"username"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName" `
	Phone     string `json:"phone"`
	Address   string `json:"address"`
	Email     string `json:"email" `
}

type UserRequest struct {
	Username  string `json:"username,omitempty" bson:"username,omitempty"`
	FirstName string `json:"firstName,omitempty" bson:"firstName,omitempty"`
	LastName  string `json:"lastName,omitempty" bson:"lastName,omitempty"`
	Phone     string `json:"phone,omitempty" bson:"phone,omitempty"`
	Address   string `json:"address,omitempty" bson:"address,omitempty"`
	Email     string `json:"email,omitempty" bson:"email,omitempty"`
}

type DataUserResponse struct {
	DataUser []UsersResponse `json:"dataUser,omitempty"`
}
