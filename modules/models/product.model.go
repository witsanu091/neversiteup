package neversiteupModels

type ProductResponse struct {
	Id          string `json:"_id" bson:"_id"`
	ProductName string `json:"productName" bson:"productName"`
	Price       int    `json:"price" bson:"price"`
	Amount      int    `json:"amount" bson:"amount"`
	Detail      string `json:"detail" bson:"detail"`
	Category    string `json:"category" bson:"category"`
	Color       string `json:"color" bson:"color"`
	Status      string `json:"status" bson:"status"`
}

type ProductRequest struct {
	Id          string `json:"_id,omitempty" bson:"_id,omitempty"`
	ProductName string `json:"productName"  validate:"required" bson:"productName"`
	Price       int    `json:"price" validate:"required" bson:"price"`
	Amount      int    `json:"amount" validate:"required" bson:"amount"`
	Detail      string `json:"detail" validate:"required" bson:"detail"`
	Category    string `json:"category" validate:"required" bson:"category"`
	Color       string `json:"color,omitempty"  validate:"required" bson:"color,omitempty"`
	Status      string `json:"status" validate:"required" bson:"status"`
}

type DataProductResponse struct {
	ProductList []ProductResponse `json:"productList,omitempty"`
}
