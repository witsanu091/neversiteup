package routes

import (
	"fmt"
	"log"
	"neversiteup/database"
	routes "neversiteup/modules/routes"
	"os"

	fiber "github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"

	"github.com/spf13/viper"
)

func StartServer() {
	a := os.Args
	env := "local"
	if len(a) > 1 {
		env = os.Args[1]
	}

	viper.Set("env", env)
	viper.SetConfigFile("./configs/" + env + ".yml")
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println("error")
	}

	app := fiber.New()
	app.Use(recover.New(
		recover.Config{
			EnableStackTrace: true,
		},
	))

	app.Use(cors.New())

	err := database.InitMongoDB()
	if err != nil {
		fmt.Println(err.Error())

	}

	err = database.InitialPostgresDatabase()
	if err != nil {
		fmt.Println(err.Error())
	}

	port := viper.GetString("service.port")

	if port == "" {
		log.Fatal("$PORT must be set")
	}

	if err != nil {
		panic(err)
	} else {
		api := app.Group("api")
		version := api.Group("v1")
		routes.UserHandlerRoute(version)
		routes.ProductHandlerRoute(version)
		routes.AuthHandlerRoute(version)
		log.Fatal(app.Listen(":" + port))
	}

}
