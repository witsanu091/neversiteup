FROM golang:1.20


RUN apt-get update && apt-get install -y git

WORKDIR /main

RUN git clone https://gitlab.com/witsanu091/neversiteup.git --branch main ./

RUN go build -o main

EXPOSE 8080

ENTRYPOINT [ "./main" ]


