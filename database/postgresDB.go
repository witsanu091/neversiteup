package database

import (
	"context"
	"database/sql"
	"log"
	"time"

	_ "github.com/lib/pq"

	"github.com/spf13/viper"
)

var postgres *sql.DB

func InitialPostgresDatabase() (err error) {

	if postgres != nil {
		postgres = nil
	}

	_, cancel := context.WithTimeout(context.Background(), 30*time.Second)

	conn, err := sql.Open(viper.GetString("db.postgres.driverName"), viper.GetString("db.postgres.dataSourceName"))
	conn.SetMaxOpenConns(10)
	conn.SetMaxIdleConns(5)
	conn.SetConnMaxLifetime(time.Second * 10)
	conn.SetConnMaxIdleTime(time.Second * 10)
	if err != nil {
		log.Println(" | InitialPostgresDatabase Failed | MSG : ", err.Error())
		defer cancel()
		return
	}

	if err = conn.Ping(); err != nil {
		log.Println(" | InitialPostgresDatabase Ping Failed | MSG : ", err.Error())
		defer cancel()
		return
	}
	defer cancel()
	postgres = conn
	log.Println(" | InitialPostgresDatabase Success ")
	return
}

func GetPostgres() *sql.DB {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	if postgres != nil {
		if err := postgres.PingContext(ctx); err != nil {
			defer postgres.Close()
			InitialPostgresDatabase()
		}
	} else {
		InitialPostgresDatabase()
	}

	return postgres
}
